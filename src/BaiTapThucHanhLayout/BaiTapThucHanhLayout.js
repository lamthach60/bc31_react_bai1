import React, { Component } from "react";
import BaitapNavbar from "./BaiTapNavbar/BaitapNavbar";
import Header from "./BaiTapHeader/Header";
import Content from "./BaiTapContent/Content";
import Footer from "./BaiTapFooter/Footer";
class BaiTapThucHanhLayout extends Component {
  render() {
    return (
      <>
        <BaitapNavbar />
        <Header />
        <Content />
        <Footer />
      </>
    );
  }
}

export default BaiTapThucHanhLayout;
